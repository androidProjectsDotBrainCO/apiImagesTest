from django.http import JsonResponse
from rest_framework.decorators import api_view
from django.views.decorators.csrf import csrf_exempt

from .models import Image

from .serializers import ImageSerializer

@csrf_exempt
def post_image(req):
    print(req.POST, req.FILES, req.GET)
    if req.method == "POST":
        print("POST")
        serializer = ImageSerializer(data=req.FILES)
        if serializer.is_valid():
            imagen = req.FILES.get('image')
            print(imagen)
            image = Image(image=imagen)
            image.save()
            return JsonResponse({'ok': 'ok', 'id': image.id}, safe=False)
        else:
            return JsonResponse({'error': 'no valid data'}, safe=False)
    else:
        return JsonResponse({'ok': 'ok'}, safe=False)