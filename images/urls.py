from django.conf.urls import url

from .views import post_image

urlpatterns = [
    url(r'^post/image/$', post_image),
]